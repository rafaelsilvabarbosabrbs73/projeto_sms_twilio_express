/// <reference types="cypress" />

describe('Projeto twilio API sms ExpressJS - Envio e digitação do SMS', () => {
    before(function () {
        cy.visit('/')
        cy.fixture("example.json").then(function (data) {
            this.data = data;
        })
    })
    it('teste de envio do sms', function() {
        cy.get('#to').type(this.data.to)
        cy.get('#body').type(this.data.body)        
        cy.get('.sms-form > button:nth-child(3)').click()
    })
    
    it('Validar envio do SMS - GET', () => {
        cy.request({
            method: 'GET',
            url: '/'
        }).then((response) => {
            expect(response.status).equal(200)            
            //expect(response.body).equal('Seu código de validação de sms é 546702')
            response = JSON.stringify(response)

        })
    });
    
})