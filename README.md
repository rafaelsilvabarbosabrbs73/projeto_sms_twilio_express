# React aplicativo com servidor Express

Este projeto foi inicializado com [Create React App] (https://github.com/facebookincubator/create-react-app). Em seguida, um servidor Express foi adicionado ao diretório `server`. O servidor é proxy através da chave `proxy` em` package.json`.

## Usando este projeto

1. Clone o projeto, mude para o diretório e instale as dependências.

   `` `bash
   git clone https://github.com/philnash/react-express-starter.git
   react-express-starter cd
   npm install
   `` `

2. Crie um arquivo `.env` para variáveis ​​de ambiente em seu servidor.

   `` `bash
   toque em .env
   `` `

3. Inicie o servidor

   Você pode iniciar o servidor sozinho com o comando:

   `` `bash
   servidor npm run
   `` `

   Execute o aplicativo React sozinho com o comando:

   `` `bash
   npm start
   `` `

   Execute os dois aplicativos junto com o comando:

   `` `bash
   npm run dev
   `` `

   O aplicativo React será executado na porta 3000 e na porta do servidor 3001.

## Starter React Twilio

O [ramotwilio] (https://github.com/philnash/react-express-starter/tree/twilio) é uma configuração semelhante, mas também fornece endpoints com [tokens de acesso] básicos (https://www.twilio.com / docs / iam / access-tokens) para [Twilio Programmable Chat] (https://www.twilio.com/docs/chat) e [Twilio Programmable Video] (https://www.twilio.com/docs/video ) Você pode usar o projeto como base para construir aplicativos de chat ou vídeo React.